#Beta
vec=c(dbeta(.9, 1.7, 1.2), pbeta(.9, 1.7, 1.2), qbeta(.95, 1.7, 1.2));
#binomial
vec=c(vec, dbinom(1., 23., .3), pbinom(1., 23., .3), qbinom(.95, 23., .3));
#Cauchy
vec=c(vec, dcauchy(1., 1.5, .3), pcauchy(1., 1.5, .3), qcauchy(.95, 1.5, .3));
#Chisq
vec=c(vec, dchisq(1., 5.), pchisq(1., 5.), qchisq(.95, 5.));
#Exp
vec=c(vec, dexp(1., 1./1.5), pexp(1., 1./1.5), qexp(.95, 1./1.5));
#F
vec=c(vec, df(1., 2., 5.), pf(1., 2., 5.), qf(.95, 2., 5.));
#Gamma
vec=c(vec, dgamma(1., shape=1.5, scale=.3), pgamma(1., shape=1.5, scale=.3), qgamma(.95, shape=1.5, scale=.3));
#Geometric
vec=c(vec, dgeom(1., .7), pgeom(1., .7), qgeom(.95, .7));
#Hypergeometric
vec=c(vec, dhyper(1., 8., 5., 4.), phyper(1., 8., 5., 4.), qhyper(.95, 8., 5., 4.));
#Logistic
vec=c(vec, dlogis(1., 1.5, .3), plogis(1., 1.5, .3), qlogis(.95, 1.5, .3));
#Lognormal
vec=c(vec, dlnorm(1., .5, .3), plnorm(1., .5, .3), qlnorm(.95, .5, .3));
#Negative binomial
vec=c(vec, dnbinom(41., 23., .3), pnbinom(41., 23., .3), qnbinom(.95, 23., .3));
#Noncentral beta
vec=c(vec, dbeta(.9, 1.7, 1.2, .8), pbeta(.9, 1.7, 1.2, .8), qbeta(.95, 1.7, 1.2, .8));
#Noncentral chisq
vec=c(vec, dchisq(1., 5., .8), pchisq(1., 5., .8), qchisq(.95, 5., .8));
#Noncentral F
vec=c(vec, df(1., 2., 5., 1.), pf(1., 2., 5., 1.), qf(.95, 2., 5., 1.));
#Noncentral Student
vec=c(vec, dt(1., 5., .8), pt(1., 5., .8), qt(.95, 5., .8));
#Normal
vec=c(vec, dnorm(1., .8, 1.1), pnorm(1., .8, 1.1), qnorm(.95, .8, 1.1));
#Poisson
vec=c(vec, dpois(1., .8), ppois(1., .8), qpois(.95, .8));
#Uniform
vec=c(vec, dunif(1., .2, 2.3), punif(1., .2, 2.3), qunif(.95, .2, 2.3));
#Weibull
vec=c(vec, dweibull(1., 1.5, .3), pweibull(1., 1.5, .3), qweibull(.95, 1.5, .3));

mat=matrix(vec, c(20, 3), byrow=T);
write.csv(mat, file="valR.csv", row.names=F);

