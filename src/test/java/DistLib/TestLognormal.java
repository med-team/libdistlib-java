/*
 * Created on Aug 1, 2020
 */
package DistLib;

import junit.framework.TestCase;


public class TestLognormal extends TestCase {
  static final double tol = 1e-6;
  
  public void testDensity() {
    assertEquals(RTestValues.getRTestValues("lognormal")[0],lognormal.density(1., .5, .3),tol);
  }
  
  public void testCumulative() {
    assertEquals(RTestValues.getRTestValues("lognormal")[1],lognormal.cumulative(1., .5, .3),tol);
  }
  
  public void testQuantile() {
    assertEquals(RTestValues.getRTestValues("lognormal")[2],lognormal.quantile(.95, .5, .3),tol);
  }


}
