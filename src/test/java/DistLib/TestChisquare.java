/*
 * Created on Aug 1, 2020
 */
package DistLib;

import junit.framework.TestCase;


public class TestChisquare extends TestCase {

  static final double tol = 1e-6;
  
  public void testDensity() {
    assertEquals(RTestValues.getRTestValues("chisq")[0],chisquare.density(1., 5.),tol);
  }
  
  public void testCumulative() {
    assertEquals(RTestValues.getRTestValues("chisq")[1],chisquare.cumulative(1., 5.),tol);
  }
  
  public void testQuantile() {
    assertEquals(RTestValues.getRTestValues("chisq")[2],chisquare.quantile(.95, 5.),tol);
  }

}
