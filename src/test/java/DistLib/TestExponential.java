/*
 * Created on Aug 1, 2020
 */
package DistLib;

import junit.framework.TestCase;


public class TestExponential extends TestCase {
  static final double tol = 1e-6;
  
  public void testDensity() {
    assertEquals(RTestValues.getRTestValues("exponential")[0],exponential.density(1., 1.5),tol);
  }
  
  public void testCumulative() {
    assertEquals(RTestValues.getRTestValues("exponential")[1],exponential.cumulative(1., 1.5),tol);
  }
  
  public void testQuantile() {
    assertEquals(RTestValues.getRTestValues("exponential")[2],exponential.quantile(.95, 1.5),tol);
  }

}
