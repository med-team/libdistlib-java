/*
 * Created on Aug 1, 2020
 */
package DistLib;

import junit.framework.TestCase;


public class TestLogistic extends TestCase {
  static final double tol = 1e-6;
  
  public void testDensity() {
    assertEquals(RTestValues.getRTestValues("logistic")[0],logistic.density(1., 1.5, .3),tol);
  }
  
  public void testCumulative() {
    assertEquals(RTestValues.getRTestValues("logistic")[1],logistic.cumulative(1., 1.5, .3),tol);
  }
  
  public void testQuantile() {
    assertEquals(RTestValues.getRTestValues("logistic")[2],logistic.quantile(.95, 1.5, .3),tol);
  }

}
