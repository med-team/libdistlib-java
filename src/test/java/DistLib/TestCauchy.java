/*
 * Created on Aug 1, 2020
 */
package DistLib;

import junit.framework.TestCase;


public class TestCauchy extends TestCase {
  static final double tol = 1e-6;
  
  public void testDensity() {
    assertEquals(RTestValues.getRTestValues("cauchy")[0],cauchy.density(1., 1.5, .3),tol);
  }
  
  public void testCumulative() {
    assertEquals(RTestValues.getRTestValues("cauchy")[1],cauchy.cumulative(1., 1.5, .3),tol);
  }
  
  public void testQuantile() {
    assertEquals(RTestValues.getRTestValues("cauchy")[2],cauchy.quantile(.95, 1.5, .3),tol);
  }

}
