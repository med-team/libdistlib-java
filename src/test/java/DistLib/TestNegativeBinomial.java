/*
 * Created on Aug 1, 2020
 */
package DistLib;

import junit.framework.TestCase;


public class TestNegativeBinomial extends TestCase {
  static final double tol = 1e-6;
  
  public void testDensity() {
    assertEquals(RTestValues.getRTestValues("negative binomial")[0],negative_binomial.density(41., 23., .3),tol);
  }
  
  public void testCumulative() {
    assertEquals(RTestValues.getRTestValues("negative binomial")[1],negative_binomial.cumulative(41., 23., .3),tol);
  }
  
  public void testQuantile() {
    assertEquals(RTestValues.getRTestValues("negative binomial")[2],negative_binomial.quantile(.95, 23., .3),tol);
  }

}
