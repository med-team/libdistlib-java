/*
 * Created on Aug 1, 2020
 */
package DistLib;

import junit.framework.TestCase;


public class TestNoncentralF extends TestCase {
  static final double tol = 1e-6;
    
  public void testCumulative() {
    assertEquals(RTestValues.getRTestValues("noncentral F")[1],noncentral_f.cumulative(1., 2., 5., 1.),tol);
  }
  
}
