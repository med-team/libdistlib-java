/*
 * Created on Apr 15, 2007
 */
package DistLib;

import junit.framework.TestCase;


public class TestNormal extends TestCase {

  static final double tol = 1e-6;

  public void testDensity() {
    assertEquals(RTestValues.getRTestValues("normal")[0],normal.density(1., .8, 1.1),tol);
  }
  

  public void testCumulative() {
    assertEquals(0.85083004,normal.cumulative(1.04, 0.0, 1.0),tol);
    assertEquals(0.99752292,normal.cumulative(2.81, 0.0, 1.0),tol);
    assertEquals(RTestValues.getRTestValues("normal")[1],normal.cumulative(1., .8, 1.1),tol);
  }
  
  public void testQuantile() {
    assertEquals(RTestValues.getRTestValues("normal")[2],normal.quantile(.95, .8, 1.1),tol);
  }

}
