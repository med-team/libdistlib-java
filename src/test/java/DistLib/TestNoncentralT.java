/*
 * Created on Aug 1, 2020
 */
package DistLib;

import junit.framework.TestCase;


public class TestNoncentralT extends TestCase {
  static final double tol = 1e-6;
    
  public void testCumulative() {
    assertEquals(RTestValues.getRTestValues("noncentral t")[1],noncentral_t.cumulative(1., 5., .8),tol);
  }
  
}
