/*
 * Created on Aug 1, 2020
 */
package DistLib;

import junit.framework.TestCase;


public class TestNoncentralBeta extends TestCase {
  static final double tol = 1e-6;
  
  public void testDensity() {
    assertEquals(RTestValues.getRTestValues("noncentral beta")[0],noncentral_beta.density(.9, 1.7, 1.2, .8),tol);
  }
  
  public void testCumulative() {
    assertEquals(RTestValues.getRTestValues("noncentral beta")[1],noncentral_beta.cumulative(.9, 1.7, 1.2, .8),tol);
  }
  
}
