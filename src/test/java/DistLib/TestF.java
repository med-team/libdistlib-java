/*
 * Created on Aug 1, 2020
 */
package DistLib;

import junit.framework.TestCase;


public class TestF extends TestCase {
  static final double tol = 1e-6;
  
  public void testDensity() {
    assertEquals(RTestValues.getRTestValues("F")[0],f.density(1., 2., 5.),tol);
  }
  
  public void testCumulative() {
    assertEquals(RTestValues.getRTestValues("F")[1],f.cumulative(1., 2., 5.),tol);
  }
  
  public void testQuantile() {
    assertEquals(RTestValues.getRTestValues("F")[2],f.quantile(.95, 2., 5.),tol);
  }

}
