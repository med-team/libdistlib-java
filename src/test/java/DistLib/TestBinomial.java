/*
 * Created on Aug 1, 2020
 */
package DistLib;

import junit.framework.TestCase;


public class TestBinomial extends TestCase {

  static final double tol = 1e-6;
  
  public void testDensity() {
    assertEquals(RTestValues.getRTestValues("binomial")[0],binomial.density(1., 23., .3),tol);
  }
  
  public void testCumulative() {
    assertEquals(RTestValues.getRTestValues("binomial")[1],binomial.cumulative(1., 23., .3),tol);
  }
  
  public void testQuantile() {
    assertEquals(RTestValues.getRTestValues("binomial")[2],binomial.quantile(.95, 23., .3),tol);
  }

}
