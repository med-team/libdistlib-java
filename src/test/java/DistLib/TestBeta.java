/*
 * Created on Aug 1, 2020
 */
package DistLib;

import junit.framework.TestCase;


public class TestBeta extends TestCase {

  static final double tol = 1e-6;
  
  public void testDensity() {
    assertEquals(RTestValues.getRTestValues("beta")[0],beta.density(.9, 1.7, 1.2),tol);
  }
  
  public void testCumulative() {
    assertEquals(RTestValues.getRTestValues("beta")[1],beta.cumulative(.9, 1.7, 1.2),tol);
  }
  
  public void testQuantile() {
    assertEquals(RTestValues.getRTestValues("beta")[2],beta.quantile(.95, 1.7, 1.2),tol);
  }
}
