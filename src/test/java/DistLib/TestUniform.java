/*
 * Created on Aug 1, 2020
 */
package DistLib;

import junit.framework.TestCase;


public class TestUniform extends TestCase {
  static final double tol = 1e-6;
  
  public void testDensity() {
    assertEquals(RTestValues.getRTestValues("uniform")[0],uniform.density(1., .2, 2.3),tol);
  }
  
  public void testCumulative() {
    assertEquals(RTestValues.getRTestValues("uniform")[1],uniform.cumulative(1., .2, 2.3),tol);
  }
  
  public void testQuantile() {
    assertEquals(RTestValues.getRTestValues("uniform")[2],uniform.quantile(.95, .2, 2.3),tol);
  }

}
