/*
 * Created on Aug 1, 2020
 */
package DistLib;

import junit.framework.TestCase;


public class TestGamma extends TestCase {
  static final double tol = 1e-6;
  
  public void testDensity() {
    assertEquals(RTestValues.getRTestValues("gamma")[0],gamma.density(1., 1.5, .3),tol);
  }
  
  public void testCumulative() {
    assertEquals(RTestValues.getRTestValues("gamma")[1],gamma.cumulative(1., 1.5, .3),tol);
  }
  
  public void testQuantile() {
    assertEquals(RTestValues.getRTestValues("gamma")[2],gamma.quantile(.95, 1.5, .3),tol);
  }

}
