/*
 * Created on Aug 1, 2020
 */
package DistLib;

import junit.framework.TestCase;


public class TestGeometric extends TestCase {
  static final double tol = 1e-6;
  
  public void testDensity() {
    assertEquals(RTestValues.getRTestValues("geometric")[0],geometric.density(1., .7),tol);
  }
  
  public void testCumulative() {
    assertEquals(RTestValues.getRTestValues("geometric")[1],geometric.cumulative(1., .7),tol);
  }
  
  public void testQuantile() {
    assertEquals(RTestValues.getRTestValues("geometric")[2],geometric.quantile(.95, .7),tol);
  }

}
