/*
 * Created on Aug 1, 2020
 */
package DistLib;

import junit.framework.TestCase;


public class TestHypergeometric extends TestCase {
  static final double tol = 1e-6;
  
  public void testDensity() {
    assertEquals(RTestValues.getRTestValues("hypergeometric")[0],hypergeometric.density(1., 8., 5., 4.),tol);
  }
  
  public void testCumulative() {
    assertEquals(RTestValues.getRTestValues("hypergeometric")[1],hypergeometric.cumulative(1., 8., 5., 4.),tol);
  }
  
  public void testQuantile() {
    assertEquals(RTestValues.getRTestValues("hypergeometric")[2],hypergeometric.quantile(.95, 8., 5., 4.),tol);
  }

}
