/*
 * Created on Aug 1, 2020
 */
package DistLib;

import junit.framework.TestCase;


public class TestWeibull extends TestCase {
  static final double tol = 1e-6;
  
  public void testDensity() {
    assertEquals(RTestValues.getRTestValues("weibull")[0],weibull.density(1., 1.5, .3),tol);
  }
  
  public void testCumulative() {
    assertEquals(RTestValues.getRTestValues("weibull")[1],weibull.cumulative(1., 1.5, .3),tol);
  }
  
  public void testQuantile() {
    assertEquals(RTestValues.getRTestValues("weibull")[2],weibull.quantile(.95, 1.5, .3),tol);
  }

}
