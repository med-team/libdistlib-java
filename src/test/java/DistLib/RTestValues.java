/*
 * Created on Aug 1, 2020
 */
package DistLib;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.StringTokenizer;

/**
 * Holds test values scanned from R output file. 
 * 
 * @author peter
 */
public class RTestValues {
  
  private static HashMap<String,double[]> rTestValues = null;
  
  /**
   * Parse 3 doubles from line.
   * 
   * @param line
   * @return array of doubles
   */
  private static double[] parseDoubles(final String line) {
    StringTokenizer tok=new StringTokenizer(line, ",");
    double[] vals = new double[3];
    for (int i = 0; i<3; i++) {
      try {
        vals[i] = Double.parseDouble(tok.nextToken());
      } catch (NumberFormatException nfe) {
        vals[i] = Double.NaN;
      }
    }
    return vals;
  }
  
  /**
   * Obtain values for named test. 
   * 
   * @param name of test
   * @return array of doubles
   */
  public static final double[] getRTestValues(final String name) {
    if (rTestValues==null) {
      try {
      BufferedReader fic = new BufferedReader(new FileReader("src/test/resources/valR.csv"));
      
      rTestValues = new HashMap<String,double[]>();
      
      fic.readLine(); //Header line.
      
      // beta - density, cumulative, quantile
      double[] vals = parseDoubles(fic.readLine());
      rTestValues.put("beta", vals);
      
      // binomial - density, cumulative, quantile
      vals = parseDoubles(fic.readLine());
      rTestValues.put("binomial", vals);

      // cauchy - density, cumulative, quantile
      vals = parseDoubles(fic.readLine());
      rTestValues.put("cauchy", vals);

      // chisq - density, cumulative, quantile
      vals = parseDoubles(fic.readLine());
      rTestValues.put("chisq", vals);

      // exponential - density, cumulative, quantile
      vals = parseDoubles(fic.readLine());
      rTestValues.put("exponential", vals);

      // F - density, cumulative, quantile
      vals = parseDoubles(fic.readLine());
      rTestValues.put("F", vals);

      // gamma - density, cumulative, quantile
      vals = parseDoubles(fic.readLine());
      rTestValues.put("gamma", vals);

      // geometric - density, cumulative, quantile
      vals = parseDoubles(fic.readLine());
      rTestValues.put("geometric", vals);
      
      // hypergeometric - density, cumulative, quantile
      vals = parseDoubles(fic.readLine());
      rTestValues.put("hypergeometric", vals);
      
      // logistic - density, cumulative, quantile
      vals = parseDoubles(fic.readLine());
      rTestValues.put("logistic", vals);

      // lognormal - density, cumulative, quantile
      vals = parseDoubles(fic.readLine());
      rTestValues.put("lognormal", vals);

      // negative binomial - density, cumulative, quantile
      vals = parseDoubles(fic.readLine());
      rTestValues.put("negative binomial", vals);
      
      // noncentral beta - density, cumulative, quantile
      vals = parseDoubles(fic.readLine());
      rTestValues.put("noncentral beta", vals);

      // noncentral chisqq - density, cumulative, quantile
      vals = parseDoubles(fic.readLine());
      rTestValues.put("noncentral chisq", vals);
      
      // noncentral F - cumulative
      vals = parseDoubles(fic.readLine());
      rTestValues.put("noncentral F", vals);
      
      // noncentral t - cumulative
      vals = parseDoubles(fic.readLine());
      rTestValues.put("noncentral t", vals);

      // normal - density, cumulative, quantile
      vals = parseDoubles(fic.readLine());
      rTestValues.put("normal", vals);
      
      // poisson - density, cumulative, quantile
      vals = parseDoubles(fic.readLine());
      rTestValues.put("poisson", vals);

      // uniform - density, cumulative, quantile
      vals = parseDoubles(fic.readLine());
      rTestValues.put("uniform", vals);

      // weibull - density, cumulative, quantile
      vals = parseDoubles(fic.readLine());
      rTestValues.put("weibull", vals);
      
      fic.close();
      } catch (IOException ioe) {
        return null;
      }
    }
    return rTestValues.get(name);
  }
  
}
