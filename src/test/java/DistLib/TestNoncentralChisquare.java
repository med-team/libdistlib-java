/*
 * Created on Aug 1, 2020
 */
package DistLib;

import junit.framework.TestCase;


public class TestNoncentralChisquare extends TestCase {
  static final double tol = 1e-6;
  
  public void testDensity() {
    assertEquals(RTestValues.getRTestValues("noncentral chisq")[0],noncentral_chisquare.density(1., 5., .8),tol);
  }
  
  public void testCumulative() {
    assertEquals(RTestValues.getRTestValues("noncentral chisq")[1],noncentral_chisquare.cumulative(1., 5., .8),tol);
  }
  
  public void testQuantile() {
    assertEquals(RTestValues.getRTestValues("noncentral chisq")[2],noncentral_chisquare.quantile(.95, 5., .8),tol);
  }

}
