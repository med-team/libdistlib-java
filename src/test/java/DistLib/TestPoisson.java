/*
 * Created on Aug 1, 2020
 */
package DistLib;

import junit.framework.TestCase;


public class TestPoisson extends TestCase {
  static final double tol = 1e-6;
  
  public void testDensity() {
    assertEquals(RTestValues.getRTestValues("poisson")[0],poisson.density(1., .8),tol);
  }
  
  public void testCumulative() {
    assertEquals(RTestValues.getRTestValues("poisson")[1],poisson.cumulative(1., .8),tol);
  }
  
  public void testQuantile() {
    assertEquals(RTestValues.getRTestValues("poisson")[2],poisson.quantile(.95, .8),tol);
  }

}
