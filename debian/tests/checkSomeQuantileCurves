#!/bin/sh
# autopkgtest check: compute values of quantiles for some beta, gamma, and
# noncentral ChiSquare distributions, and compare them to the ones that are
# outputted by R.
# (C) 2020 Pierre Gruet.
# Author: Pierre Gruet <pgt@debian.org>

set -e

SOURCEDIR=$(pwd)
WORKDIR=$(mktemp -d)
trap "rm -rf $WORKDIR" 0 INT QUIT ABRT PIPE TERM
cd $WORKDIR

#Create the R code that will output the quantiles values.
cat <<EOF > stat.r
#Beta
vec=c(qbeta(seq(0.05, 0.95, 0.05), 0.8, 0.24));
#Gamma
vec=c(vec, qgamma(seq(0.05, 0.95, 0.05), shape=0.25, scale=1.3));
#Noncentral chisq
vec=c(vec, qchisq(seq(0.05, 0.95, 0.05), 8, 1.29));
mat=matrix(vec, c(19, 3), byrow=F);
write.csv(mat, file="valR.csv", row.names=F);
EOF

# Create the Java program that will load the R file with reference values, call
# the functions of the library and check the computed values.
cat <<EOF > CompareOutputs.java
import java.io.*;
import java.lang.*;
import java.util.*;
import DistLib.beta;
import DistLib.gamma;
import DistLib.noncentral_chisquare;

public class CompareOutputs
{
        public static void main(String[] args) throws IOException
        {
                BufferedReader fic = new BufferedReader(new FileReader("valR.csv"));
                StringTokenizer tok;
                boolean errorMet=false;
                double expected, got;
                
                fic.readLine(); //Header line.
                
                /*
                 * For each value q=0.05, 0.10, ..., 0.95, we read a line with 
                 * the q-quantiles of Beta(0.8, 0.24),
                 *                    Gamma(shape=0.25, scale=1.3),
                 *                    Noncentral ChiSquare(8, 1.29).
                 */
                for (double q=0.05 ; q<0.95+1.E-10 ; q+=0.05) {
                        tok=new StringTokenizer(fic.readLine(), ",");
                        expected=Double.parseDouble(tok.nextToken());
                        got=beta.quantile(q, 0.8, 0.24);
                        if (Math.abs(expected-got)>1.E-6) {
                                System.out.println("Error on beta quantile at " + q + ": expected " + expected + ", got " + got);
                                errorMet=true;
                        }
                        expected=Double.parseDouble(tok.nextToken());
                        got=gamma.quantile(q, 0.25, 1.3);
                        if (Math.abs(expected-got)>1.E-6) {
                                System.out.println("Error on gamma quantile at " + q + ": expected " + expected + ", got " + got);
                                errorMet=true;
                        }
                        expected=Double.parseDouble(tok.nextToken());
                        got=noncentral_chisquare.quantile(q, 8., 1.29);
                        if (Math.abs(expected-got)>1.E-6) {
                                System.out.println("Error on noncentral ChiSquare quantile at " + q + ": expected " + expected + ", got " + got);
                                errorMet=true;
                        }
                }
                
                fic.close();
                
                if (errorMet)
                        System.exit(1);
                else
                        System.out.println("Tests pass.");
        }
}
EOF

R --slave --no-save < stat.r

export CLASSPATH=".:/usr/share/java/distlib.jar"
javac CompareOutputs.java
java CompareOutputs

