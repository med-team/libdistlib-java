libdistlib-java (1.0-5) unstable; urgency=medium

  * Raising Standards version to 4.7.0 (no change)
  * Using log4j for the tests, previously it was skipped
  * Removing obsolete Lintian override about bad-jar-name
  * Suggesting the -doc package instead of recommending it
  * Using Maven substvars instead of javahelper ones

 -- Pierre Gruet <pgt@debian.org>  Fri, 17 May 2024 22:52:39 +0200

libdistlib-java (1.0-4) unstable; urgency=medium

  * Raising Standards version to 4.6.1 (no change)
  * Reworking the -doc Lintian overrides after a syntax change in Lintian
  * Adding a Lintian override for the jar name

 -- Pierre Gruet <pgt@debian.org>  Sat, 08 Oct 2022 14:43:37 +0200

libdistlib-java (1.0-3) unstable; urgency=medium

  * Raising Standards version to 4.6.0 (no change)
  * Simplifying d/rules
  * Adding a Lintian override for bash term detected in Java code

 -- Pierre Gruet <pgt@debian.org>  Fri, 04 Mar 2022 17:34:52 +0100

libdistlib-java (1.0-2) unstable; urgency=medium

  * Adding missing classpath in manifest
  * Marking the -doc package as Multi-Arch:foreign
  * Deleting useless override of dh_auto_clean

 -- Pierre Gruet <pgtdebian@free.fr>  Mon, 14 Sep 2020 17:18:54 +0200

libdistlib-java (1.0-1) unstable; urgency=medium

  * New upstream version 1.0
  * Deleting patches taken into account in new upstream version
  * Adapting debian/watch to new path upstream
  * Now building with Maven, as pom.xml is provided by upstream
  * Deleting a test from autopkgtest as upstream build-time test files changed
  * Updating debian/copyright
  * Set upstream metadata fields: Repository and Bug-Database.

 -- Pierre Gruet <pgtdebian@free.fr>  Wed, 12 Aug 2020 22:27:52 +0200

libdistlib-java (0.9.1+dfsg-2) unstable; urgency=medium

  * Team upload.
  * debhelper-compat 13 (routine-update)
  * Rules-Requires-Root: no (routine-update)

 -- Andreas Tille <tille@debian.org>  Sun, 12 Jul 2020 22:39:52 +0200

libdistlib-java (0.9.1+dfsg-1) unstable; urgency=medium

  * Initial Debian upload, version 0.9.1+dfsg (Closes: #961158)

 -- Pierre Gruet <pgtdebian@free.fr>  Mon, 25 May 2020 22:19:38 +0200
